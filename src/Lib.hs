{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Lib
    ( getEventIdList
    , getMembersIdList
    , getAttendanceForEvent
    , parseAttendanceForEvent
    , getMemberNoShow
    , countOcc
    ) where

import Control.Lens

import Data.Aeson           (Value, FromJSON, ToJSON)
import Data.Aeson.Lens      (key, _String, _Integer, values)
import Data.Aeson.Types     hiding (Options)

import Data.List            as L
import Data.Maybe
import Data.Map             as Map
import Data.Text            as T
import Data.Tuple.Extra     (fst3)

import GHC.Generics
import Network.Wreq
import System.Environment

type Isnoshow             = Bool
type UrlName              = String
type EventId              = String
type MemberId             = String
type GroupName            = Text
type Resp                 = Response (Map String Value)
type EventAttResp         = Response [Map String Value]
type Counter k v          = Map.Map k v

eventsOptions :: GroupName
              -> Options
eventsOptions groupName = defaults
  & param "format" .~ ["json"]
  & param "group_urlname" .~ [groupName]
  & param "status" .~ ["past"]
  & param "order" .~ ["time"]
  & param "page" .~ ["100"]

membersOptions :: GroupName
              -> Options
membersOptions groupName = defaults
  & param "format" .~ ["json"]
  & param "group_urlname" .~ [groupName]
  & param "order" .~ ["name"]
  & param "page" .~ ["100"]

eventAttendanceOptions :: Options
eventAttendanceOptions = defaults
  & param "order" .~ ["time"]
  & param "page" .~ ["100"]

meetupEventsUrl :: String
meetupEventsUrl = "https://api.meetup.com/2/events?key="

getEventIdList :: GroupName -> IO [EventId]
getEventIdList groupName = do
  apiKey <- getEnv "API_KEY"
  r <- asJSON =<< getWith
                (eventsOptions groupName)
                  (meetupEventsUrl ++ apiKey) :: IO Resp
  let respBody :: Maybe (Map String Value) = r ^? responseBody
  let results :: Value = fromJust $ Map.lookup "results" (fromJust respBody)
  let value :: [Text] = results ^.. values.key "id" . _String
  return $ (L.map T.unpack value)

meetupMembersUrl :: String
meetupMembersUrl = "https://api.meetup.com/2/members?key="

getMembersIdList :: GroupName -> IO [MemberId]
getMembersIdList groupName = do
  apiKey <- getEnv "API_KEY"
  r <- asJSON =<< getWith
                (membersOptions groupName)
                  (meetupMembersUrl ++ apiKey) :: IO Resp
  let respBody :: Maybe (Map String Value) = r ^? responseBody
  let results :: Value = fromJust $ Map.lookup "results" (fromJust respBody)
  let value :: [Integer] = results ^.. values.key "id" . _Integer
  return $ fmap show value

eventAttendanceUrl :: String
eventAttendanceUrl = "https://api.meetup.com/"

getAttendanceForEvent ::  UrlName -> EventId -> IO [(Value, Value, Maybe Value)]
getAttendanceForEvent urlName eventsId = do
  apiKey <- getEnv "API_KEY"
  r <- asJSON =<< getWith eventAttendanceOptions
                  (eventAttendanceUrl ++ urlName ++
                      "/events/" ++ eventsId ++ "/attendance?key=" ++
                          apiKey) :: IO EventAttResp
  let respBody :: Maybe [Map String Value] = r ^? responseBody
  let rsvp :: [Value] = fmap (fromJust . Map.lookup "rsvp")
                                      (fromJust respBody)
  let status :: [Maybe Value] = fmap (Map.lookup "status") (fromJust respBody)
  let member :: [Value] = fmap (fromJust . Map.lookup "member") (fromJust respBody)
  let result :: [(Value, Value, Maybe Value)] = L.zip3 member rsvp status
  return result

data Photo = Photo {
  id :: Int,
  highres_link :: String,
  photo_link :: String,
  thumb_link :: String,
  -- type :: String,
  base_url :: String }
  deriving (Show, Generic, ToJSON, FromJSON)

newtype EventContext = EventContext {
  host :: Bool }
  deriving (Show, Generic, ToJSON, FromJSON)

data Member = Member {
  id :: Int,
  name :: String }
  --photo :: Photo,
  --role :: Maybe String,
  --event_context :: EventContext }
  deriving (Ord, Show, Generic, ToJSON, FromJSON)

data Rsvp = Rsvp {
  id :: Int,
  response :: String,
  guests :: Int,
  updated :: Int }
  deriving (Show, Generic, ToJSON, FromJSON)

data Attendance = Attendance {
  member :: Member,
  rsvp :: Rsvp,
  attendance_id :: Int,
  status :: String,
  updated :: Int,
  guests :: Int }
  deriving (Show, Generic, ToJSON, FromJSON)

parseAttendanceForEvent :: UrlName -> EventId -> IO [(Member, Rsvp, Isnoshow)]
parseAttendanceForEvent urlName eventsId = do
  result <- getAttendanceForEvent urlName eventsId
  let (jmember :: [Value], jrsvp :: [Value], jstatus :: [Maybe Value]) =
          L.unzip3 result
  let (Success res_member) = traverse fromJSON jmember :: Result [Member]
  let (Success res_rsvp) = traverse fromJSON jrsvp :: Result [Rsvp]
  let is_noshow = fmap isJust jstatus
  return $ L.zip3 res_member res_rsvp is_noshow

isNoShow :: (Member, Rsvp, Isnoshow) -> Bool
isNoShow (_, (Rsvp _ resp _ _), isnoshow) = (isnoshow == True) && (resp == "yes")

getMemberNoShow :: UrlName -> EventId -> IO [Member]
getMemberNoShow urlName eventId = do
  r <- parseAttendanceForEvent urlName eventId
  let membersNoShow = fmap fst3 $ L.filter isNoShow r
  return membersNoShow

instance Eq Member where
  (==) (Member id1 name1) (Member id2 name2) = (id1 == id2) && (name1 == name2)

-- Taken from https://github.com/wei2912/counter/blob/master/src/Data/Counter.hs
cupdateWith :: (Ord k, Num v) => k -> v -> Counter k v -> Counter k v
cupdateWith = Map.insertWith (+)

cupdate :: (Ord k, Num v) => k -> Counter k v -> Counter k v
cupdate k = cupdateWith k 1

countOcc :: (Ord k, Num v) => [k] -> Counter k v
countOcc = L.foldl' (flip cupdate) Map.empty
