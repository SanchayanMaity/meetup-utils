# meetup-utils

This is a small utility code which given a meetup group name gets the
name of members and number of no shows from each member. It is my first
project in Haskell and perhaps I will work on improving this a bit more
,but, for now it gives me the result I want.

Before running this, the API key provided by meetup has to exported from
the command line as below.

##### `export API_KEY=your_numeric_key`

To run the program, the meetup group name can be provided from the
command line.

##### `stack exec -- meetup-utils-exe --meetupname="rustox"`
