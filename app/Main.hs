{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveDataTypeable #-}

module Main where

import   Lib
import   Control.Monad            (join)
import   Data.List                as L (sortOn)
import   Data.Map                 as Map (toList)
import   Data.Text                as T (pack)
import   Text.Show.Pretty         (ppShow)
import   System.Console.CmdArgs

data Group = Group { meetupname :: String
                   } deriving (Show, Data, Typeable)

options :: Group
options = Group { meetupname = "Bangalore-Functional-Programmers-Meetup"
                }

main :: IO ()
main = do
  op <- cmdArgs options
  el <- getEventIdList (T.pack $ meetupname op)
  r <- traverse (getMemberNoShow (meetupname op)) el
  let result = L.sortOn snd $ Map.toList $ countOcc (join r)
  putStrLn $ ppShow result
  return ()
